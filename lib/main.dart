import 'package:flutter/material.dart';
// import 'package:geolocator/geolocator.dart';
import 'package:geolocation/geolocation.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: MyHomePage(title: 'Location Finder'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // int _counter = 0;
  var _location;
  // var _geoLocator = Geolocator();
  // var _locationOptions = LocationOptions(accuracy: LocationAccuracy.high);

  @override
  void initState() {
    super.initState();
    checkGPS();
  }

  checkGPS() async {
    final GeolocationResult result = await Geolocation.isLocationOperational();
    if (result.isSuccessful) {
      print('Success');
    } else {
      print('Failed');
    }
    // Position position = await _geoLocator.getCurrentPosition(
    //     desiredAccuracy: LocationAccuracy.high);
    // setState(() {
    //   _location = "Latitude: " +
    //       position.latitude.toString() +
    //       " Longitude: " +
    //       position.longitude.toString();
    // });
  }

  void _incrementCounter() {
    // setState(() {
    //   // _counter--;
    //   _location =
    // });
    getLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Current Location:',
            ),
            Text(
              '$_location',
              style: Theme.of(context).textTheme.body2,
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }

  getLocation() async {
    // Position position = await _geoLocator.getCurrentPosition(
    //     desiredAccuracy: LocationAccuracy.high);
    // setState(() {
    //   _location = "Latitude: " +
    //       position.latitude.toString() +
    //       " Longitude: " +
    //       position.longitude.toString();
    // });
    // return 'Hello';
  }
}
